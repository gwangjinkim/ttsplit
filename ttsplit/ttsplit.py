import pandas as pd
import numpy as np
import math

class ttsplit:
    """
    The Train Test splitter.
    
    from traintestsplit import TrainTestSplit as tts
    train_indexes, test_indexes = tts.tratified_split_indexes(labels, ratio=0.1, seed=1)
    **kwargs are for pandas .sample() method!
    """
    @classmethod
    def sample(cls, l, k, replace=True, seed=1,**kwargs):
        # random_state is seed
        return [x for x in pd.DataFrame({'idx': l})['idx'].sample(k, replace=replace, random_state=seed, **kwargs)]
    
    @classmethod
    def split(cls, l, ratio, seed=1, **kwargs):
        n, k = len(l), math.floor(ratio*len(l))
        res = cls.sample(l, n, replace=False, seed=seed, **kwargs)
        return res[k:n], res[:k]
    
    @classmethod
    def stratified_split_indexes(cls, labels, ratio=0.1, seed=1, **kwargs):
        def flatten(l): return [x for y in l for x in y]
        universe = sorted(list(set(labels)))
        labels = np.array(labels)
        indexes_list = [np.where(labels == u)[0] for u in universe]
        split_list = [cls.split(il, ratio=ratio, seed=seed, **kwargs) for il in indexes_list]
        train_indexes = flatten([x[0] for x in split_list])
        test_indexes = flatten([x[1] for x in split_list])
        return train_indexes, test_indexes
    
    @classmethod
    def select(cls, l, indexes):
        return [l[i] for i in indexes]
    
    @classmethod
    def stratified_split(cls, df, labels=None, ratio=0.1, seed=1, **kwargs):
        if labels is None:
            labels = df.shape[0] * [1] # take entire data as one entire "class" ~ non-stratified
        train_indexes, test_indexes = cls.stratified_split_indexes(labels=labels,
                                                                  ratio=ratio,
                                                                  seed=seed,
                                                                  **kwargs)
        X_train, X_test = df.iloc[train_indexes, :], df.iloc[test_indexes, :]
        y_train, y_test = cls.select(labels, train_indexes), cls.select(labels, test_indexes)
        return X_train, X_test, y_train, y_test, train_indexes, test_indexes
