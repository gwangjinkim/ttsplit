from setuptools import setup

setup(
    name="ttsplit",
    version="0.0.1",
    description="train test split for machine/deep learning",
    packages=["ttsplit"],
    install_requires=["pandas", "numpy"],
    author="Gwang-Jin Kim",
    author_email="gwang.jin.kim.phd@gmail.com",
    keywords=["machine learning", "train test split"],
    url="https://bitbucket.org/gwangjinkim/ttsplit",
    license='MIT'
)
