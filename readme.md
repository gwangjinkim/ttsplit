# Installation

```
pip install -e 'git+https://gwangjinkim@bitbucket.org/gwangjinkim/ttsplit.git#egg=ttsplit'
```

# Usage

```
from ttsplit import ttsplit as tts

# split only the lables and get the train_indexes and test_indexes
train_indexes, test_indexes = tts.stratified_split_indexes(labels, ratio=0.1, seed=1, **kwargs)
## **kwargs are the arguments for the pandas .sample() method

# split from data frame df and labels by:
X_train, X_test, y_train, y_test, train_indexes, test_indexes = tts.stratified_split(df, labels, ratio=0.2, seed=1)

# to do non-stratified split, assign `labels = None`.
X_train, X_test, y_train, y_test, train_indexes, test_indexes = tts.stratified_split(df, labels=None, ratio=0.2, seed=1)
```


